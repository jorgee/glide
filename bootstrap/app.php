<?php

use Glide\Application\Http\Middlewares\ExpandParamParser;
use Glide\Application\Services\HttpRequestValidation\RequestValidator;
use Glide\Client\HttpClient;
use Glide\Domain\Repositories\DepartmentFileRepository;
use Glide\Domain\Repositories\EmployeeApiRepository;
use Glide\Domain\Repositories\Interfaces\DepartmentRepository;
use Glide\Domain\Repositories\Interfaces\EmployeeRepository;
use Glide\Domain\Repositories\Interfaces\OfficeRepository;
use Glide\Domain\Repositories\OfficeFileRepository;
use Glide\Domain\Repositories\RelationsBuilder;
use Glide\Domain\Services\Departments as DepartmentsDomainService;
use Glide\Domain\Services\Employees as EmployeesDomainService;
use Glide\Domain\Services\ExpansionRequestValidator\ExpansionRequestValidator;
use Glide\Domain\Services\Offices as OfficesDomainService;
use Glide\Infrastructure\Services\DataSources\JsonFileDataSource;
use GuzzleHttp\Client;

require_once __DIR__ . '/../vendor/autoload.php';

$envFile = null;
if (env('APP_ENV') === 'testing') {
    $envFile = '.env.testing';
}
(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__),
    $envFile
))->bootstrap();

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    Glide\Application\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->routeMiddleware([
    'param.expand.parser' => ExpandParamParser::class,
]);


/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/
$app->register(RequestValidator::class);
$app->register(Glide\Providers\AppServiceProvider::class);


$repositoriesConfig = config('repositories');
$officesRepository = new OfficeFileRepository(new JsonFileDataSource($repositoriesConfig['offices_json']));
$departmentsRepository = new DepartmentFileRepository(
    new JsonFileDataSource($repositoriesConfig['departments_json'])
);
$employeesRepository = new EmployeeApiRepository(
    new HttpClient(
        new Client(
            ['base_uri' => $repositoriesConfig['employees_api_base_url']]
        )
    )
);

RelationsBuilder::setEntityRepository('superdepartment', $departmentsRepository);
RelationsBuilder::setEntityRepository('department', $departmentsRepository);
RelationsBuilder::setEntityRepository('office', $officesRepository);
RelationsBuilder::setEntityRepository('employee', $employeesRepository);
RelationsBuilder::setEntityRepository('manager', $employeesRepository);

$app->when(ExpansionRequestValidator::class)
    ->needs('$allowedExpansions')
    ->give(config('allowedExpansions'));

$app->when(DepartmentsDomainService::class)
    ->needs(DepartmentRepository::class)
    ->give(function () use ($departmentsRepository) {
        return $departmentsRepository;
    });

$app->when(OfficesDomainService::class)
    ->needs(OfficeRepository::class)
    ->give(function () use ($officesRepository) {
        return $officesRepository;
    });

$app->when(EmployeesDomainService::class)
    ->needs(EmployeeRepository::class)
    ->give(function () use ($employeesRepository) {
        return $employeesRepository;
    });

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'Glide\Application\Controllers\Http',
], function ($router) {
    require __DIR__ . '/../config/routes/api.php';
});

return $app;
