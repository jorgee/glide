<?php

namespace Glide\Client;

use Exception;
use Glide\Client\Exceptions\HttpClientException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

class HttpClient implements HttpClientInterface
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * HttpClient constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $uri
     * @param mixed $body
     * @param array $headers
     *
     * @return mixed
     * @throws HttpClientException
     */
    public function get(string $uri, $body, array $headers = [])
    {
        return $this->handleRequest('GET', $uri, $body, $headers);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param mixed $body
     * @param array $headers
     *
     * @return mixed
     * @throws HttpClientException
     */
    private function handleRequest(string $method, string $uri, $body, array $headers = [])
    {
        try {
            $response = $this->client->request(
                $method,
                $uri,
                [
                    'json' => $body,
                    'headers' => $headers
                ]
            );

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            throw new HttpClientException(
                $e->getResponse()->getBody()->getContents(),
                $e->getResponse()->getStatusCode()
            );
        } catch (ServerException $e) {
            throw new HttpClientException($e->getMessage(), $e->getResponse()->getStatusCode());
        } catch (GuzzleException $e) {
            throw new HttpClientException($e->getMessage());
        } catch (Exception $e) {
            throw new HttpClientException($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function post(string $uri, $body, array $headers = [])
    {
        // TODO: Implement post() method.
    }

    /**
     * @inheritDoc
     */
    public function delete(string $uri, $body, array $headers = [])
    {
        // TODO: Implement delete() method.
    }
}
