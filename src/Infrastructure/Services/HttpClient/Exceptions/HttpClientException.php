<?php

namespace Glide\Client\Exceptions;

use Exception;

class HttpClientException extends Exception
{
    /**
     * @var int
     */
    protected $httpStatus;

    /**
     * HttpClientException constructor.
     *
     * @param string $message
     * @param int $status
     * @param int $code
     */
    public function __construct(string $message = "", $status = 200, int $code = 0)
    {
        parent::__construct($message, $code);
        $this->httpStatus = $status;
    }

    /**
     * @return mixed
     */
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * @var int $status
     *
     * @return void
     */
    public function setHttpStatus(int $status)
    {
        $this->httpStatus = $status;
    }
}
