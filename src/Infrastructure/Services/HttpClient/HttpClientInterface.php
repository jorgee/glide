<?php

namespace Glide\Client;

use Glide\Client\Exceptions\HttpClientException;

interface HttpClientInterface
{
    /**
     * @param string $uri
     * @param mixed  $body
     * @param array  $headers
     *
     * @return array|null
     * @throws HttpClientException
     */
    public function get(string $uri, $body, array $headers = []);

    /**
     * @param string $uri
     * @param mixed  $body
     * @param array  $headers
     *
     * @return array|null
     * @throws HttpClientException
     */
    public function post(string $uri, $body, array $headers = []);

    /**
     * @param string $uri
     * @param mixed  $body
     * @param array  $headers
     *
     * @return array|null
     * @throws HttpClientException
     */
    public function delete(string $uri, $body, array $headers = []);
}
