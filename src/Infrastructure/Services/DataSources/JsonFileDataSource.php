<?php

namespace Glide\Infrastructure\Services\DataSources;

use Exception;

/**
 * Class JsonFileLoader
 * @package Glide\Infrastructure\Services\DataSources
 */
class JsonFileDataSource extends FileDataSource
{
    /**
     * @param string $filePath
     * @param int $offset ignored because JSON file cannot be read partially.
     * @param int|null $length ignored because JSON file cannot be read partially.
     * @return null|array
     * @throws Exception
     */
    public function load(string $filePath = null, int $offset = 0, int $length = null)
    {
        $data = parent::load($filePath, 0, null);
        if (!$data) {
            throw new Exception('Invalid empty JSON data in file.');
        }
        return json_decode($data, true);
    }
}
