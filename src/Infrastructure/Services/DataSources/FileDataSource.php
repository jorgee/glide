<?php

namespace Glide\Infrastructure\Services\DataSources;

use Exception;

/**
 * Class FileLoader
 * @package Glide\Infrastructure\Services\DataSources
 */
class FileDataSource
{
    /**
     * @var string
     */
    protected $filePath;

    /**
     * FileDataSource constructor.
     * @param string|null $filePath
     */
    public function __construct(string $filePath = null)
    {
        $this->filePath = $filePath;
    }

    /**
     * @param string $filePath
     * @param int $offset
     * @param int|null $length
     * @return false|string
     * @throws Exception
     */
    public function load(string $filePath = null, int $offset = 0, int $length = null)
    {
        if (!$filePath && !$this->filePath) {
            throw new Exception('File path not set for File Data Source.');
        }
        $filePath = $filePath ?? $this->filePath;
        if (!is_file($filePath)) {
            throw new Exception(sprintf('Not such file \'%s\'', $filePath));
        }
        return file_get_contents($filePath);
    }
}
