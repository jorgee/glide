<?php

namespace Glide\Domain\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Requests\Internal\DepartmentService\GetAllDepartmentsRequest;
use Glide\Application\Requests\Internal\DepartmentService\GetDepartmentRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Service;
use Glide\Domain\Repositories\Interfaces\DepartmentRepository;
use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;
use Glide\Domain\Services\ExpansionRequestValidator\ExpansionRequestValidator;
use Glide\Domain\Traits\ExpansionValidatorTrait;

/**
 * Class Departments
 */
class Departments extends Service
{
    use ExpansionValidatorTrait;

    /**
     * @var DepartmentRepository
     */
    protected $departmentRepository;

    public function __construct(
        ResponseFactory $responseFactory,
        DepartmentRepository $departmentRepository,
        ExpansionRequestValidator $expansionRequestValidator
    ) {
        $this->departmentRepository = $departmentRepository;
        $this->expansionRequestValidator = $expansionRequestValidator;
        parent::__construct($responseFactory);
    }

    /**
     * @param GetAllDepartmentsRequest $request
     * @return InternalApiResponse
     * @throws InvalidExpansionRequestException
     */
    public function getAll(GetAllDepartmentsRequest $request)
    {
        $this->validateExpansionOrFail($request->getExpansions(), 'department');

        $departments = $this->departmentRepository
            ->withRelations($request->getExpansions())
            ->all($request->getOffset(), $request->getLimit())
            ->fetch();

        return $this->success(Errors::ERR_NO_ERROR_MESSAGE, $departments);
    }

    /**
     * @param GetDepartmentRequest $request
     * @return InternalApiResponse
     * @throws InvalidExpansionRequestException
     */
    public function getById(GetDepartmentRequest $request): InternalApiResponse
    {
        $this->validateExpansionOrFail($request->getExpansions(), 'department');

        $data = $this->departmentRepository
            ->withRelations($request->getExpansions())
            ->withIds([$request->getId()])
            ->fetch();

        if (is_array($data) && count($data) === 1) {
            return $this->success(Errors::ERR_NO_ERROR_MESSAGE, $data[0]);
        }

        return $this->error(
            Errors::ERR_NOT_FOUND_MESSAGE,
            Errors::ERR_NOT_FOUND_CODE,
            [
                sprintf('No department found with id \'%d\'.', $request->getId())
            ]
        );
    }
}
