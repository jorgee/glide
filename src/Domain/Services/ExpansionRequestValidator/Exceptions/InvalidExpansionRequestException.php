<?php

namespace Glide\Domain\Services\ExpansionRequestValidator\Exceptions;

use Exception;
use Throwable;

/**
 * Class InvalidExpansionRequestException
 * @package Glide\Domain\Services\ExpansionRequestValidator\Exceptions
 */
class InvalidExpansionRequestException extends Exception
{

    /**
     * @var string[]
     */
    protected $errors;

    public function __construct($errors, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
