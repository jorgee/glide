<?php

namespace Glide\Domain\Services\ExpansionRequestValidator;

use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Service;

/**
 * Class ExpansionRequestValidator
 * Checks if an expansion is correctly formatted.
 */
class ExpansionRequestValidator extends Service
{
    /**
     * @var array
     */
    protected $allowedExpansions = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * ExpansionRequestValidator constructor.
     * @param array $allowedExpansions
     * @param ResponseFactory $responseFactory
     */
    public function __construct(array $allowedExpansions, ResponseFactory $responseFactory)
    {
        $this->allowedExpansions = $allowedExpansions;
        parent::__construct($responseFactory);
    }

    /**
     * Check if the requested expansions are allowed
     * @param $entity
     * @param array $expansions
     * @return bool
     */
    public function expansionsAllowed($entity, array $expansions): bool
    {
        $this->errors = [];
        foreach ($expansions as $requestedExpansion) {
            $segments = explode('.', $requestedExpansion);
            for ($i = 0; $i < count($segments); $i++) {
                $currentSegment = $segments[$i];
                $previousSegment = null;
                if ($i >= 1) {
                    $previousSegment = $segments[$i - 1];
                }
                // First entity in expansion request.
                if ($previousSegment === null && !$this->entityCanExpandEntity($entity, $currentSegment)) {
                    $this->addForbiddenExpansionError($entity, $currentSegment, $requestedExpansion);
                }
                // Following entities in expansion request.
                // Get unaliased entity name (eg. super department is a department)
                $previousSegmentUnaliased = isset($this->allowedExpansions[$entity][$previousSegment])
                    ? $this->allowedExpansions[$entity][$previousSegment]
                    : $previousSegment;
                if (
                    $previousSegment !== null
                    && !$this->entityCanExpandEntity($previousSegmentUnaliased, $currentSegment)
                ) {
                    $this->addForbiddenExpansionError($previousSegment, $currentSegment, $requestedExpansion);
                }
            }
        }

        return count($this->getErrors()) === 0;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $error
     */
    protected function addError(string $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @param string $entity
     * @param string $followingEntity
     * @param string $requestedExpansion
     */
    protected function addForbiddenExpansionError(
        string $entity,
        string $followingEntity,
        string $requestedExpansion
    ): void {
        $this->addError(
            sprintf(
                'The entity \'%s\' cannot expand \'%s\' in the expansion request \'%s\'.',
                $followingEntity,
                $entity,
                $requestedExpansion
            )
        );
    }

    /**
     * Check if an entity is allowed to expand another entity.
     * @param string $segment
     * @param string $followerSegment
     * @return bool
     */
    protected function entityCanExpandEntity(string $segment, string $followerSegment): bool
    {
        return array_key_exists($segment, $this->allowedExpansions)
            && isset($this->allowedExpansions[$segment][$followerSegment]);
    }
}
