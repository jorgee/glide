<?php

namespace Glide\Domain\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Requests\Internal\OfficeService\GetAllOfficesRequest;
use Glide\Application\Requests\Internal\OfficeService\GetOfficeRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Service;
use Glide\Domain\Repositories\Interfaces\OfficeRepository;
use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;
use Glide\Domain\Services\ExpansionRequestValidator\ExpansionRequestValidator;
use Glide\Domain\Traits\ExpansionValidatorTrait;

/**
 * Class OfficesService
 */
class Offices extends Service
{
    use ExpansionValidatorTrait;

    /**
     * @var OfficeRepository
     */
    protected $officeRepository;

    /**
     * Offices constructor.
     * @param ResponseFactory $responseFactory
     * @param OfficeRepository $officeRepository
     * @param ExpansionRequestValidator $expansionRequestValidator
     */
    public function __construct(
        ResponseFactory $responseFactory,
        OfficeRepository $officeRepository,
        ExpansionRequestValidator $expansionRequestValidator
    ) {
        $this->officeRepository = $officeRepository;
        $this->expansionRequestValidator = $expansionRequestValidator;
        parent::__construct($responseFactory);
    }

    /**
     * @param GetAllOfficesRequest $request
     * @return InternalApiResponse
     * @throws InvalidExpansionRequestException
     */
    public function getAll(GetAllOfficesRequest $request)
    {
        $this->validateExpansionOrFail($request->getExpansions(), 'office');

        $offices = $this->officeRepository
            ->withRelations($request->getExpansions())
            ->all($request->getOffset(), $request->getLimit())
            ->fetch();

        return $this->success(Errors::ERR_NO_ERROR_MESSAGE, $offices);
    }

    /**
     * @param GetOfficeRequest $request
     * @return InternalApiResponse
     * @throws InvalidExpansionRequestException
     */
    public function getById(GetOfficeRequest $request): InternalApiResponse
    {
        $this->validateExpansionOrFail($request->getExpansions(), 'office');

        $data = $this->officeRepository
            ->withRelations($request->getExpansions())
            ->withIds([$request->getId()])
            ->fetch();

        if (is_array($data) && count($data) === 1) {
            return $this->success(Errors::ERR_NO_ERROR_MESSAGE, $data[0]);
        }

        return $this->error(
            Errors::ERR_NOT_FOUND_MESSAGE,
            Errors::ERR_NOT_FOUND_CODE,
            [
                sprintf('No office found with id \'%d\'.', $request->getId())
            ]
        );
    }
}
