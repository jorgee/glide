<?php

namespace Glide\Domain\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Requests\Internal\EmployeeService\GetAllEmployeesRequest;
use Glide\Application\Requests\Internal\EmployeeService\GetEmployeeRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Service;
use Glide\Domain\Repositories\Interfaces\EmployeeRepository;
use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;
use Glide\Domain\Services\ExpansionRequestValidator\ExpansionRequestValidator;
use Glide\Domain\Traits\ExpansionValidatorTrait;

/**
 * Class Employees
 */
class Employees extends Service
{
    use ExpansionValidatorTrait;

    /**
     * @var EmployeeRepository
     */
    protected $employeeRepository;

    /**
     * @var ExpansionRequestValidator
     */
    protected $expansionRequestValidator;

    /**
     * Employees constructor.
     * @param ResponseFactory $responseFactory
     * @param EmployeeRepository $employeeRepository
     * @param ExpansionRequestValidator $expansionRequestValidator
     */
    public function __construct(
        ResponseFactory $responseFactory,
        EmployeeRepository $employeeRepository,
        ExpansionRequestValidator $expansionRequestValidator
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->expansionRequestValidator = $expansionRequestValidator;
        parent::__construct($responseFactory);
    }

    /**
     * @param GetAllEmployeesRequest $request
     * @return InternalApiResponse
     * @throws InvalidExpansionRequestException
     */
    public function getAll(GetAllEmployeesRequest $request)
    {
        $this->validateExpansionOrFail($request->getExpansions(), 'employee');

        $employees = $this->employeeRepository
            ->withRelations($request->getExpansions())
            ->all($request->getOffset(), $request->getLimit())
            ->fetch();

        return $this->success(Errors::ERR_NO_ERROR_MESSAGE, $employees);
    }

    /**
     * @param GetEmployeeRequest $request
     * @return InternalApiResponse
     * @throws InvalidExpansionRequestException
     */
    public function getById(GetEmployeeRequest $request): InternalApiResponse
    {
        $this->validateExpansionOrFail($request->getExpansions(), 'employee');

        $data = $this->employeeRepository
            ->withRelations($request->getExpansions())
            ->withIds([$request->getId()])
            ->fetch();

        if (is_array($data) && count($data) === 1) {
            return $this->success(Errors::ERR_NO_ERROR_MESSAGE, $data[0]);
        }

        return $this->error(
            Errors::ERR_NOT_FOUND_MESSAGE,
            Errors::ERR_NOT_FOUND_CODE,
            [
                sprintf('No employee found with id \'%d\'.', $request->getId())
            ]
        );
    }
}
