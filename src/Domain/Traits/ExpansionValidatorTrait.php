<?php

namespace Glide\Domain\Traits;

use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;
use Glide\Domain\Services\ExpansionRequestValidator\ExpansionRequestValidator;

trait ExpansionValidatorTrait
{
    /**
     * @var ExpansionRequestValidator
     */
    protected $expansionRequestValidator;

    /**
     * @param array $expansionRequest
     * @param string $entityName
     * @return bool
     * @throws InvalidExpansionRequestException
     */
    protected function validateExpansionOrFail(array $expansionRequest, string $entityName): bool
    {
        $hasValidExpansions = $this->expansionRequestValidator->expansionsAllowed(
            $entityName,
            $expansionRequest
        );

        if (!$hasValidExpansions) {
            throw new InvalidExpansionRequestException(
                $this->expansionRequestValidator->getErrors(),
                'Invalid entity expansion request(s) detected.'
            );
        }

        return true;
    }
}
