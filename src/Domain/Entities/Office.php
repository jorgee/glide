<?php

namespace Glide\Domain\Entities;

use Glide\Application\Interfaces\Arrayable;

class Office implements Arrayable
{
    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * @var string;
     */
    protected $city = '';

    /**
     * @var string
     */
    protected $country = '';

    /**
     * @var string
     */
    protected $address = '';

    /**
     * Office constructor.
     * @param int|null $id
     * @param string $city
     * @param string $country
     * @param string $address
     */
    public function __construct(?int $id, string $city, string $country, string $address)
    {
        $this->id = $id;
        $this->city = $city;
        $this->country = $country;
        $this->address = $address;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'city' => $this->getCity(),
            'country' => $this->getCountry(),
            'address' => $this->getAddress()
        ];
    }
}
