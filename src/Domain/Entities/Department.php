<?php

namespace Glide\Domain\Entities;

use Glide\Application\Interfaces\Arrayable;
use JsonSerializable;

/**
 * Class Department
 * @package Glide\Domain\Entities
 */
class Department implements Arrayable, JsonSerializable
{
    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var Department|null
     */
    protected $superdepartment = null;

    /**
     * @var int|null
     */
    protected $superdepartmentId = null;

    /**
     * Department constructor.
     * @param int|null $id
     * @param string $name
     * @param Department|null $superdepartment
     * @param int|null $superdepartmentId
     */
    public function __construct(?int $id, string $name, ?Department $superdepartment, ?int $superdepartmentId)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setSuperdepartment($superdepartment);
        $this->setSuperdepartmentId($superdepartmentId);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Department|null
     */
    public function getSuperdepartment(): ?Department
    {
        return $this->superdepartment;
    }

    /**
     * @param Department|null $superdepartment
     */
    public function setSuperdepartment(?Department $superdepartment): void
    {
        $this->superdepartment = $superdepartment;
        if ($this->superdepartment) {
            $this->setSuperdepartmentId($superdepartment->getId());
        }
    }

    /**
     * @return int|null
     */
    public function getSuperdepartmentId(): ?int
    {
        return $this->superdepartmentId;
    }

    /**
     * @param int|null $superdepartmentId
     */
    public function setSuperdepartmentId(?int $superdepartmentId): void
    {
        // Ensure data consistency
        if ($this->superdepartment !== null && $this->superdepartment->getId() !== $superdepartmentId) {
            $this->superdepartment = null;
        }
        $this->superdepartmentId = $superdepartmentId;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'superdepartment' => $this->getSuperdepartment() ?? $this->getSuperdepartmentId(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
