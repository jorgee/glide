<?php

namespace Glide\Domain\Entities;

use Glide\Application\Interfaces\Arrayable;

/**
 * Class Employee
 * @package Glide\Domain\Entities
 */
class Employee implements Arrayable
{
    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * @var string
     */
    protected $first = '';

    /**
     * @var string
     */
    protected $last = '';

    /**
     * @var Employee|null
     */
    protected $manager = null;

    /**
     * @var int|null
     */
    protected $managerId = null;

    /**
     * @var Department|null
     */
    protected $department = null;

    /**
     * @var int|null
     */
    protected $departmentId = null;

    /**
     * @var Office|null
     */
    protected $office = null;

    /**
     * @var int|null
     */
    protected $officeId = null;

    /**
     * Employee constructor.
     * @param int|null $id
     * @param string $first
     * @param string $last
     * @param Employee|null $manager
     * @param int|null $managerId
     * @param Department|null $department
     * @param int|null $departmentId
     * @param Office|null $office
     * @param int|null $officeId
     */
    public function __construct(
        ?int $id,
        string $first,
        string $last,
        ?Employee $manager,
        ?int $managerId,
        ?Department $department,
        ?int $departmentId,
        ?Office $office,
        ?int $officeId
    ) {
        $this->id = $id;
        $this->first = $first;
        $this->last = $last;
        $this->manager = $manager;
        $this->managerId = $managerId;
        $this->department = $department;
        $this->departmentId = $departmentId;
        $this->office = $office;
        $this->officeId = $officeId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirst(): string
    {
        return $this->first;
    }

    /**
     * @param string $first
     */
    public function setFirst(string $first): void
    {
        $this->first = $first;
    }

    /**
     * @return string
     */
    public function getLast(): string
    {
        return $this->last;
    }

    /**
     * @param string $last
     */
    public function setLast(string $last): void
    {
        $this->last = $last;
    }

    /**
     * @return Employee|null
     */
    public function getManager(): ?Employee
    {
        return $this->manager;
    }

    /**
     * @param Employee|null $manager
     */
    public function setManager(?Employee $manager): void
    {
        $this->manager = $manager;
        $this->setManagerId($manager->getId());
    }

    /**
     * @return int|null
     */
    public function getManagerId(): ?int
    {
        return $this->managerId;
    }

    /**
     * @param int|null $managerId
     */
    public function setManagerId(?int $managerId): void
    {
        // Ensure data consistency
        if ($this->manager !== null && $this->manager->getId() !== $managerId) {
            $this->manager = null;
        }
        $this->managerId = $managerId;
    }

    /**
     * @return Department|null
     */
    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    /**
     * @param Department|null $department
     */
    public function setDepartment(?Department $department): void
    {
        $this->department = $department;
        $this->setDepartmentId($department->getId());
    }

    /**
     * @return int|null
     */
    public function getDepartmentId(): ?int
    {
        return $this->departmentId;
    }

    /**
     * @param int|null $departmentId
     */
    public function setDepartmentId(?int $departmentId): void
    {
        // Ensure data consistency
        if ($this->department !== null && $this->department->getId() !== $departmentId) {
            $this->department = null;
        }
        $this->departmentId = $departmentId;
    }

    /**
     * @return Office|null
     */
    public function getOffice(): ?Office
    {
        return $this->office;
    }

    /**
     * @param Office|null $office
     */
    public function setOffice(?Office $office): void
    {
        $this->office = $office;
        $this->setOfficeId($office->getId());
    }

    /**
     * @return int|null
     */
    public function getOfficeId(): ?int
    {
        return $this->officeId;
    }

    /**
     * @param int|null $officeId
     */
    public function setOfficeId(?int $officeId): void
    {
        // Ensure data consistency
        if ($this->office !== null && $this->office->getId() !== $officeId) {
            $this->office = null;
        }
        $this->officeId = $officeId;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'first' => $this->getFirst(),
            'last' => $this->getLast(),
            'manager' => $this->getManager() ?? $this->getManagerId(),
            'department' => $this->getDepartment() ?? $this->getDepartmentId(),
            'office' => $this->getOffice() ?? $this->getOfficeId()
        ];
    }
}
