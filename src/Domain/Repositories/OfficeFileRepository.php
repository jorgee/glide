<?php

namespace Glide\Domain\Repositories;

use Glide\Domain\Entities\Office;
use Glide\Domain\Repositories\Interfaces\OfficeRepository;

class OfficeFileRepository extends JsonBaseRepository implements OfficeRepository
{
    /**
     * @param array $data
     * @return Office
     */
    public function arrayToEntity(array $data): Office
    {
        return new Office(
            $data['id'] ?? null,
            $data['city'] ?? null,
            $data['country'] ?? null,
            $data['address'] ?? null,
        );
    }

    /**
     * @inheritDoc
     * @return Office[]|null
     */
    public function fetch(bool $asEntity = false)
    {
        return parent::fetch($asEntity);
    }
}
