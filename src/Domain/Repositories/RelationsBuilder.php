<?php

namespace Glide\Domain\Repositories;

use Exception;
use Glide\Domain\Repositories\Interfaces\Repository;

/**
 * Class RelationsBuilders
 * @package Glide\Domain\Repositories
 */
abstract class RelationsBuilder
{
    /**
     * @var Repository[]
     */
    protected static $entitiesRepositories = [];

    /**
     * @param $entityId
     * @param $repository
     */
    public static function setEntityRepository($entityId, $repository)
    {
        self::$entitiesRepositories[$entityId] = $repository;
    }

    /**
     * @param string $relations
     * @param $dataSet
     * @throws Exception
     */
    public static function buildAndFetch(string $relations, &$dataSet)
    {
        $explodedRelations = explode('.', $relations);
        $currentRelation = $explodedRelations[0];
        $idsToFetch = [];
        $requestedEntityMap = [];
        for ($i = 0; $i < count($dataSet); $i++) {
            $currentEntityData = $dataSet[$i];
            if (isset($currentEntityData[$currentRelation])) {
                $entityId = $currentEntityData[$currentRelation];
                $idsToFetch[$entityId] = true;
                $requestedEntityMap[$entityId][] = $i;
            }
        }
        $idsToFetch = array_keys($idsToFetch);
        $fetchedData = self::getEntitiesDataById($currentRelation, $idsToFetch);
        if (count($explodedRelations) > 1) {
            unset($explodedRelations[0]);
            self::buildAndFetch(implode('.', $explodedRelations), $fetchedData);
        }
        foreach ($fetchedData as $entityData) {
            $entityId = $entityData['id'];
            $indexesThatNeedTheRelation = $requestedEntityMap[$entityId];
            foreach ($indexesThatNeedTheRelation as $index) {
                $dataSet[$index][$currentRelation] = $entityData;
            }
        }
    }

    /**
     * @param string $entityName
     * @param array $entityIds
     * @return mixed
     * @throws Exception
     */
    protected static function getEntitiesDataById(string $entityName, array $entityIds)
    {
        if (!isset(self::$entitiesRepositories[$entityName])) {
            throw new Exception(sprintf('Missing repository information for entity \'%s\'.', $entityName));
        }

        $repository = self::$entitiesRepositories[$entityName];

        return $repository->withIds($entityIds)->fetch();
    }
}
