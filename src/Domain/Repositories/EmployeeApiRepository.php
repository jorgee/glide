<?php

namespace Glide\Domain\Repositories;

use Glide\Client\Exceptions\HttpClientException;
use Glide\Client\HttpClient;
use Glide\Domain\Entities\Employee;
use Glide\Domain\Repositories\Interfaces\EmployeeRepository;

class EmployeeApiRepository extends BaseRepository implements EmployeeRepository
{

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * EmployeeApiRepository constructor.
     * @param HttpClient $httpClient
     */
    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param array $data
     * @return Employee
     */
    public function arrayToEntity(array $data): Employee
    {
        return new Employee(
            $data['id'] ?? null,
            $data['first'] ?? null,
            $data['last'] ?? null,
            null,
            $data['manager'] ?? null,
            null,
            $data['department'] ?? null,
            null,
            $data['officeId'] ?? null,
        );
    }

    /**
     * @inheritDoc
     * @throws HttpClientException
     */
    protected function getAllById(array $ids, bool $asEntity = false)
    {
        $queryString = implode('&id=', $ids);
        $queryString = $queryString ? '?id=' . $queryString : '';

        $path = '/bigcorp/employees' . $queryString;
        $found = $this->httpClient->get($path, []);
        if ($asEntity) {
            $asEntities = [];
            foreach ($found as $departmentData) {
                $asEntities[] = $this->arrayToEntity($departmentData);
            }
            return $asEntities;
        }

        return $found;
    }

    /**
     * @inheritDoc
     * @throws HttpClientException
     */
    protected function getAll(int $offset = 0, int $limit = 100, bool $asEntity = false): array
    {
        $queryString = sprintf('?limit=%d&offset=%d', $this->limit, $this->offset);

        $path = '/bigcorp/employees' . $queryString;
        $found = $this->httpClient->get($path, []);
        if ($asEntity) {
            $asEntities = [];
            foreach ($found as $departmentData) {
                $asEntities[] = $this->arrayToEntity($departmentData);
            }
            return $asEntities;
        }

        return $found;
    }
}
