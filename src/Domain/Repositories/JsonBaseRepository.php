<?php

namespace Glide\Domain\Repositories;

use Exception;
use Glide\Domain\Entities\Department;
use Glide\Domain\Repositories\Interfaces\DepartmentRepository;
use Glide\Infrastructure\Services\DataSources\JsonFileDataSource;

abstract class JsonBaseRepository extends BaseRepository implements DepartmentRepository
{
    /**
     * @var JsonFileDataSource
     */
    protected $fileDataSource;

    /**
     * @var array|null
     */
    protected $loadedData;

    /**
     * DepartmentFileRepository constructor.
     * @param JsonFileDataSource $fileDataSource
     */
    public function __construct(JsonFileDataSource $fileDataSource)
    {
        $this->fileDataSource = $fileDataSource;
    }

    /**
     * @throws Exception
     */
    protected function loadData()
    {
        if ($this->loadedData === null) {
            $this->loadedData = $this->fileDataSource->load();
        }
    }

    /**
     * @param array $ids
     * @param bool $asEntity
     * @return Department[]
     * @throws Exception
     */
    protected function getAllById(array $ids, bool $asEntity = false)
    {
        $this->loadData();

        $found = [];
        if ($this->loadedData) {
            foreach ($this->loadedData as $department) {
                if (isset($department['id']) && in_array($department['id'], $ids)) {
                    $found[] = $department;
                }
            }
        }
        if ($asEntity) {
            $asEntities = [];
            foreach ($found as $departmentData) {
                $asEntities[] = $this->arrayToEntity($departmentData);
            }
            return $asEntities;
        }

        return $found;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param bool $asEntity
     * @return array
     * @throws Exception
     */
    protected function getAll(int $offset = 0, int $limit = 100, bool $asEntity = false): array
    {
        $this->loadData();

        $found = [];
        $addedCount = 0;
        for ($i = 0; $i < count($this->loadedData); $i++) {
            if ($i < $offset) {
                continue;
            }
            $found[] = $this->loadedData[$i];
            $addedCount++;
            if ($addedCount >= $limit) {
                break;
            }
        }

        if ($asEntity) {
            $asEntities = [];
            foreach ($found as $entityData) {
                $asEntities[] = $this->arrayToEntity($entityData);
            }
            return $asEntities;
        }

        return $found;
    }
}
