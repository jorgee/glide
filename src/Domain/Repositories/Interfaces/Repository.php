<?php

namespace Glide\Domain\Repositories\Interfaces;

/**
 * Interface Repository
 * @package Glide\Domain\Repositories\Interfaces
 */
interface Repository
{
    /**
     * @param array $ids
     * @return Repository
     */
    public function withIds(array $ids): self;

    /**
     * @param int $offset
     * @param int $limit
     * @return Repository
     */
    public function all(int $offset = 0, int $limit = 100): self;

    /**
     * @param bool $asEntity
     * @return mixed
     */
    public function fetch(bool $asEntity = false);

    /**
     * @param array $relations
     * @return $this
     */
    public function withRelations(array $relations): self;
}
