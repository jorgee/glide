<?php

namespace Glide\Domain\Repositories;

use Glide\Domain\Entities\Department;

/**
 * Class Department
 */
class DepartmentFileRepository extends JsonBaseRepository
{
    /**
     * @param array $data
     * @return Department
     */
    public function arrayToEntity(array $data): Department
    {
        return new Department(
            $data['id'] ?? null,
            $data['name'] ?? null,
            null,
            $data['superdepartment'] ?? null
        );
    }

    /**
     * @inheritDoc
     * @return Department[]|null
     */
    public function fetch(bool $asEntity = false)
    {
        return parent::fetch($asEntity);
    }
}
