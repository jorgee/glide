<?php

namespace Glide\Domain\Repositories;

use Exception;
use Glide\Domain\Repositories\Interfaces\Repository;

/**
 * Class BaseRepository
 * @package Glide\Domain\Repositories
 */
abstract class BaseRepository implements Repository
{
    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $limit = 100;

    /**
     * @var array
     */
    protected $idsToFetch = [];

    /**
     * @var bool
     */
    protected $fetchAll = false;

    /**
     * @var array
     */
    protected $relationsToExpand = [];

    /**
     * @inheritDoc
     */
    public function withRelations(array $relations): Repository
    {
        $this->relationsToExpand = $relations;

        return $this;
    }

    /**
     * To be implemented by each repository. Internal usage.
     * @param array $ids
     * @param bool $asEntity
     * @return mixed
     */
    abstract protected function getAllById(array $ids, bool $asEntity = false);

    /**
     * To be implemented by each repository. Internal usage.
     * @param int $offset
     * @param int $limit
     * @param bool $asEntity
     * @return array
     */
    abstract protected function getAll(int $offset = 0, int $limit = 100, bool $asEntity = false): array;

    /**
     * Reset class properties.
     */
    protected function reset()
    {
        $this->idsToFetch = [];
        $this->offset = 0;
        $this->limit = 100;
        $this->relationsToExpand = [];
        $this->fetchAll = false;
    }

    /**
     * @inheritDoc
     * @param bool $asEntity
     * @return array|null
     * @throws Exception
     */
    public function fetch(bool $asEntity = false)
    {
        $data = null;
        if ($this->fetchAll) {
            $data = $this->getAll($this->offset, $this->limit, $asEntity);
        } elseif (isset($this->idsToFetch)) {
            $data = $this->getAllById($this->idsToFetch, $asEntity);
        }
        $relationsToExpand = $this->relationsToExpand;
        $this->reset();
        if ($data && $relationsToExpand) {
            foreach ($relationsToExpand as $expansionRequest) {
                RelationsBuilder::buildAndFetch($expansionRequest, $data);
            }
        }
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function withIds(array $ids): Interfaces\Repository
    {
        $this->idsToFetch = $ids;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function all(int $offset = 0, int $limit = 100): Interfaces\Repository
    {
        $this->fetchAll = true;
        $this->offset = $offset;
        $this->limit = $limit;
        return $this;
    }
}
