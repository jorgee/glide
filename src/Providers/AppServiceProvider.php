<?php

namespace Glide\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

/**
 * Class AppServiceProvider
 * This class will allow to read config files from config directory.
 * @package Glide\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /** @var Application $app */
        $app = $this->app;

        // Register own config files
        $files = array_diff(scandir($app->basePath('config')), ['.', '..']);
        foreach ($files as $config) {
            $app->configure(basename($config, '.php'));
        }
    }
}
