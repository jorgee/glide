<?php

namespace Glide\Application\Controllers\Http;

use Glide\Application\Requests\External\OfficeDetailFormRequest;
use Glide\Application\Requests\External\OfficeListFormRequest;
use Glide\Application\Requests\Internal\OfficeService\GetAllOfficesRequest;
use Glide\Application\Requests\Internal\OfficeService\GetOfficeRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Offices as OfficesApplicationService;
use Illuminate\Http\JsonResponse;

/**
 * Class Offices
 * @package Glide\Application\Controllers\Http
 */
class Offices extends Controller
{
    /**
     * @var OfficesApplicationService
     */
    protected $officesService;

    public function __construct(ResponseFactory $responseFactory, OfficesApplicationService $officesService)
    {
        $this->officesService = $officesService;
        parent::__construct($responseFactory);
    }

    /**
     * @param OfficeListFormRequest $request
     * @return JsonResponse
     */
    public function index(OfficeListFormRequest $request)
    {
        $internalRequest = new GetAllOfficesRequest(
            $request->input('offset', 0),
            $request->input('limit', 100),
            $request->input('expand', [])
        );
        $internalResponse = $this->officesService->getAll($internalRequest);

        if ($internalResponse->getSuccess()) {
            return $this->success('Offices fetched.', $internalResponse->getData());
        }

        return $this->error('Error fetching offices.', $internalResponse->getErrors(), 500);
    }

    /**
     * @param OfficeDetailFormRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function getById(OfficeDetailFormRequest $request, int $id)
    {
        $internalRequest = new GetOfficeRequest($id, $request->input('expand', []));
        $internalResponse = $this->officesService->getById($internalRequest);

        if ($internalResponse->getSuccess()) {
            return $this->success('Office fetched.', $internalResponse->getData());
        }

        return $this->error('Error fetching office.', $internalResponse->getErrors(), 500);
    }
}
