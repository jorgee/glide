<?php

namespace Glide\Application\Controllers\Http;

use Glide\Application\Responses\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @var ResponseFactory The Standard Response Factory.
     */
    protected $responseFactory;

    /**
     * Controller constructor.
     *
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return JsonResponse
     */
    protected function success(string $message, array $data = []): JsonResponse
    {
        $response = $this->responseFactory::makeStandardResponse(true, $data, $message);

        return response()->json($response->getResponseArray());
    }

    /**
     * @param string $message
     * @param array  $errors
     * @param int    $statusCode
     *
     * @return JsonResponse
     */
    protected function error(string $message, array $errors, int $statusCode): JsonResponse
    {
        $response = $this->responseFactory::makeStandardResponse(
            false,
            [],
            $message,
            $errors
        );

        return response()->json($response->getResponseArray(), $statusCode);
    }
}
