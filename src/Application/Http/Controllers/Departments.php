<?php

namespace Glide\Application\Controllers\Http;

use Glide\Application\Requests\External\DepartmentDetailFormRequest;
use Glide\Application\Requests\External\DepartmentListFormRequest;
use Glide\Application\Requests\Internal\DepartmentService\GetAllDepartmentsRequest;
use Glide\Application\Requests\Internal\DepartmentService\GetDepartmentRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Departments as DepartmentsApplicationService;
use Illuminate\Http\JsonResponse;

/**
 * Class Departments
 * @package Glide\Application\Controllers\Http
 */
class Departments extends Controller
{
    /**
     * @var DepartmentsApplicationService
     */
    protected $departmentsService;

    public function __construct(ResponseFactory $responseFactory, DepartmentsApplicationService $departmentsService)
    {
        $this->departmentsService = $departmentsService;
        parent::__construct($responseFactory);
    }

    /**
     * @param DepartmentListFormRequest $request
     * @return JsonResponse
     */
    public function index(DepartmentListFormRequest $request)
    {
        $internalRequest = new GetAllDepartmentsRequest(
            $request->input('offset', 0),
            $request->input('limit', 100),
            $request->input('expand', [])
        );
        $internalResponse = $this->departmentsService->getAll($internalRequest);

        if ($internalResponse->getSuccess()) {
            return $this->success('Departments fetched.', $internalResponse->getData());
        }

        return $this->error('Error fetching deparments.', $internalResponse->getErrors(), 500);
    }

    /**
     * @param DepartmentDetailFormRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function getById(DepartmentDetailFormRequest $request, int $id)
    {
        $internalRequest = new GetDepartmentRequest($id, $request->input('expand', []));
        $internalResponse = $this->departmentsService->getById($internalRequest);

        if ($internalResponse->getSuccess()) {
            return $this->success('Departments fetched.', $internalResponse->getData());
        }

        return $this->error('Error fetching deparments.', $internalResponse->getErrors(), 500);
    }
}
