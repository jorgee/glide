<?php

namespace Glide\Application\Controllers\Http;

use Glide\Application\Requests\External\EmployeeDetailFormRequest;
use Glide\Application\Requests\External\EmployeeListFormRequest;
use Glide\Application\Requests\Internal\EmployeeService\GetAllEmployeesRequest;
use Glide\Application\Requests\Internal\EmployeeService\GetEmployeeRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Services\Employees as EmployeesApplicationService;
use Illuminate\Http\JsonResponse;

/**
 * Class Employees
 * @package Glide\Application\Controllers\Http
 */
class Employees extends Controller
{
    /**
     * @var EmployeesApplicationService
     */
    protected $employeesService;

    public function __construct(ResponseFactory $responseFactory, EmployeesApplicationService $employeesService)
    {
        $this->employeesService = $employeesService;
        parent::__construct($responseFactory);
    }

    /**
     * @param EmployeeListFormRequest $request
     * @return JsonResponse
     */
    public function index(EmployeeListFormRequest $request)
    {
        $internalRequest = new GetAllEmployeesRequest(
            $request->input('offset', 0),
            $request->input('limit', 100),
            $request->input('expand', [])
        );
        $internalResponse = $this->employeesService->getAll($internalRequest);

        if ($internalResponse->getSuccess()) {
            return $this->success('Employees fetched.', $internalResponse->getData());
        }

        return $this->error('Error fetching employees.', $internalResponse->getErrors(), 500);
    }

    /**
     * @param EmployeeDetailFormRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function getById(EmployeeDetailFormRequest $request, int $id)
    {
        $internalRequest = new GetEmployeeRequest($id, $request->input('expand', []));
        $internalResponse = $this->employeesService->getById($internalRequest);

        if ($internalResponse->getSuccess()) {
            return $this->success('Employee fetched.', $internalResponse->getData());
        }

        return $this->error('Error fetching employee.', $internalResponse->getErrors(), 500);
    }
}
