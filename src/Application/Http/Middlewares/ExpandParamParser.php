<?php

namespace Glide\Application\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;

/**
 * Class ParseExpandParam
 */
class ExpandParamParser
{
    /**
     * Run the request filter.
     * This will ensure that a query string containing expand=manager&expand=office gets transformed into an array.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $query  = explode('&', $_SERVER['QUERY_STRING']);
        $params = array();

        foreach($query as $param )
        {
            if (strpos($param, '=') === false) $param .= '=';

            list($name, $value) = explode('=', $param, 2);
            $params[urldecode($name)][] = urldecode($value);
        }

        if (isset($params['expand'])) {
            $request->request->set('expand', $params['expand']);
        }

        return $next($request);
    }
}
