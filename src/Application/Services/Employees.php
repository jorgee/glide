<?php

namespace Glide\Application\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Requests\Internal\EmployeeService\GetAllEmployeesRequest;
use Glide\Application\Requests\Internal\EmployeeService\GetEmployeeRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Domain\Services\Employees as EmployeesDomainService;
use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;

/**
 * Class EmployeesService
 */
class Employees extends Service
{
    /**
     * @var EmployeesDomainService
     */
    protected $employeeDomainService;

    /**
     * Employees constructor.
     * @param ResponseFactory $responseFactory
     * @param EmployeesDomainService $employeeDomainService
     */
    public function __construct(ResponseFactory $responseFactory, EmployeesDomainService $employeeDomainService)
    {
        parent::__construct($responseFactory);
        $this->employeeDomainService = $employeeDomainService;
    }

    /**
     * @param GetAllEmployeesRequest $request
     * @return InternalApiResponse
     */
    public function getAll(GetAllEmployeesRequest $request)
    {
        try {
            $domainResponse = $this->employeeDomainService->getAll($request);

            if ($domainResponse->getSuccess()) {
                return $this->success(
                    'Employee fetched.',
                    $domainResponse->getData(),
                    $domainResponse
                );
            } else {
                return $this->error(
                    'Error fetching employees.',
                    $domainResponse->getErrorCode(),
                    $domainResponse->getErrors(),
                    $domainResponse
                );
            }
        } catch (InvalidExpansionRequestException $e) {
            return $this->error($e->getMessage(), Errors::ERR_INVALID_EXPANSION_CODE, $e->getErrors());
        }
    }

    /**
     * @param GetEmployeeRequest $request
     * @return InternalApiResponse
     */
    public function getById(GetEmployeeRequest $request)
    {
        try {
            $domainResponse = $this->employeeDomainService->getById($request);

            if ($domainResponse->getSuccess()) {
                return $this->success(
                    'Employee fetched.',
                    $domainResponse->getData(),
                    $domainResponse
                );
            } else {
                return $this->error(
                    'Error fetching employee.',
                    $domainResponse->getErrorCode(),
                    $domainResponse->getErrors(),
                    $domainResponse
                );
            }
        } catch (InvalidExpansionRequestException $e) {
            return $this->error($e->getMessage(), Errors::ERR_INVALID_EXPANSION_CODE, $e->getErrors());
        }
    }
}
