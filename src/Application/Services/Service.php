<?php

namespace Glide\Application\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Responses\ResponseFactory;

class Service
{
    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * Service constructor.
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param string $message
     * @param mixed $data
     *
     * @param InternalApiResponse|null $previous
     * @return InternalApiResponse
     */
    protected function success(
        string $message,
        $data,
        InternalApiResponse $previous = null
    ): InternalApiResponse {
        return $this->responseFactory::makeInternalApiResponse(
            true,
            $data,
            $message,
            Errors::ERR_NO_ERROR,
            [],
            $previous
        );
    }

    /**
     * @param string $message
     * @param int $errorCode
     * @param array $errors
     *
     * @param InternalApiResponse $previous
     * @return InternalApiResponse
     */
    protected function error(
        string $message,
        int $errorCode,
        array $errors,
        InternalApiResponse $previous = null
    ): InternalApiResponse {
        return $this->responseFactory::makeInternalApiResponse(false, [], $message, $errorCode, $errors, $previous);
    }
}
