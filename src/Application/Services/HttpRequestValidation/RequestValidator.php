<?php

namespace Glide\Application\Services\HttpRequestValidation;

use Glide\Application\Requests\External\FormRequest;
use Illuminate\Support\ServiceProvider;

class RequestValidator extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->app->resolving(FormRequest::class, function ($request, $app) {
            $request = FormRequest::createFrom($app['request'], $request);
        });

        $this->app->afterResolving(FormRequest::class, function (FormRequest $form) {
            $form->validate();
        });
    }
}
