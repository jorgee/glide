<?php

namespace Glide\Application\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Requests\Internal\OfficeService\GetAllOfficesRequest;
use Glide\Application\Requests\Internal\OfficeService\GetOfficeRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;
use Glide\Domain\Services\Offices as OfficesDomainService;

/**
 * Class OfficesService
 */
class Offices extends Service
{
    /**
     * @var OfficesDomainService
     */
    protected $officesDomainService;

    /**
     * Employees constructor.
     * @param ResponseFactory $responseFactory
     * @param OfficesDomainService $officesService
     */
    public function __construct(ResponseFactory $responseFactory, OfficesDomainService $officesService)
    {
        parent::__construct($responseFactory);
        $this->officesDomainService = $officesService;
    }

    /**
     * @param GetAllOfficesRequest $request
     * @return InternalApiResponse
     */
    public function getAll(GetAllOfficesRequest $request)
    {
        try {
            $domainResponse = $this->officesDomainService->getAll($request);

            if ($domainResponse->getSuccess()) {
                return $this->success(
                    'Offices fetched.',
                    $domainResponse->getData(),
                    $domainResponse
                );
            } else {
                return $this->error(
                    'Error fetching offices.',
                    $domainResponse->getErrorCode(),
                    $domainResponse->getErrors(),
                    $domainResponse
                );
            }
        } catch (InvalidExpansionRequestException $e) {
            return $this->error($e->getMessage(), Errors::ERR_INVALID_EXPANSION_CODE, $e->getErrors());
        }
    }

    /**
     * @param GetOfficeRequest $request
     * @return InternalApiResponse
     */
    public function getById(GetOfficeRequest $request)
    {
        try {
            $domainResponse = $this->officesDomainService->getById($request);

            if ($domainResponse->getSuccess()) {
                return $this->success(
                    'Office fetched.',
                    $domainResponse->getData(),
                    $domainResponse
                );
            } else {
                return $this->error(
                    'Error fetching offices.',
                    $domainResponse->getErrorCode(),
                    $domainResponse->getErrors(),
                    $domainResponse
                );
            }
        } catch (InvalidExpansionRequestException $e) {
            return $this->error($e->getMessage(), Errors::ERR_INVALID_EXPANSION_CODE, $e->getErrors());
        }
    }
}
