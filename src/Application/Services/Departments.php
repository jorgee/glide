<?php

namespace Glide\Application\Services;

use Glide\Application\Errors\Errors;
use Glide\Application\Interfaces\InternalApiResponse;
use Glide\Application\Requests\Internal\DepartmentService\GetAllDepartmentsRequest;
use Glide\Application\Requests\Internal\DepartmentService\GetDepartmentRequest;
use Glide\Application\Responses\ResponseFactory;
use Glide\Domain\Services\Departments as DepartmentsDomainService;
use Glide\Domain\Services\ExpansionRequestValidator\Exceptions\InvalidExpansionRequestException;

/**
 * Class DepartmentsService
 */
class Departments extends Service
{
    /**
     * @var DepartmentsDomainService
     */
    protected $departmentsDomainService;

    /**
     * Employees constructor.
     * @param ResponseFactory $responseFactory
     * @param DepartmentsDomainService $departmentsService
     */
    public function __construct(ResponseFactory $responseFactory, DepartmentsDomainService $departmentsService)
    {
        parent::__construct($responseFactory);
        $this->departmentsDomainService = $departmentsService;
    }

    /**
     * @param GetAllDepartmentsRequest $request
     * @return InternalApiResponse
     */
    public function getAll(GetAllDepartmentsRequest $request)
    {
        try {
            $domainResponse = $this->departmentsDomainService->getAll($request);

            if ($domainResponse->getSuccess()) {
                return $this->success(
                    'Departments fetched.',
                    $domainResponse->getData(),
                    $domainResponse
                );
            } else {
                return $this->error(
                    'Error fetching departments.',
                    $domainResponse->getErrorCode(),
                    $domainResponse->getErrors(),
                    $domainResponse
                );
            }
        } catch (InvalidExpansionRequestException $e) {
            return $this->error($e->getMessage(), Errors::ERR_INVALID_EXPANSION_CODE, $e->getErrors());
        }
    }

    /**
     * @param GetDepartmentRequest $request
     * @return InternalApiResponse
     */
    public function getById(GetDepartmentRequest $request)
    {
        try {
            $domainResponse = $this->departmentsDomainService->getById($request);

            if ($domainResponse->getSuccess()) {
                return $this->success(
                    'Department fetched.',
                    $domainResponse->getData(),
                    $domainResponse
                );
            } else {
                return $this->error(
                    'Error fetching department.',
                    $domainResponse->getErrorCode(),
                    $domainResponse->getErrors(),
                    $domainResponse
                );
            }
        } catch (InvalidExpansionRequestException $e) {
            return $this->error($e->getMessage(), Errors::ERR_INVALID_EXPANSION_CODE, $e->getErrors());
        }
    }
}
