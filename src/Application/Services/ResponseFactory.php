<?php

namespace Glide\Application\Responses;

use Glide\Application\Interfaces\HttpResponse;
use Glide\Application\Interfaces\InternalApiResponse as InternalApiResponseInterface;

class ResponseFactory
{
    /**
     * @param bool $status
     * @param mixed $data
     * @param string|null $message
     * @param int $errorCode
     * @param array $errors
     * @param InternalApiResponseInterface $previousResponse
     * @return InternalApiResponse
     */
    public static function makeInternalApiResponse(
        $status = false,
        $data = [],
        string $message = null,
        int $errorCode = 0,
        array $errors = [],
        InternalApiResponseInterface $previousResponse = null
    ): InternalApiResponse {
        return new InternalApiResponse($status, $data, $message, $errorCode, $errors, $previousResponse);
    }

    /**
     * @param bool $status
     * @param array $data
     * @param string|null $message
     * @param array $errors
     * @param int $statusCode
     * @return HttpResponse|StandardHttpResponse
     */
    public static function makeStandardResponse(
        $status = false,
        array $data = [],
        string $message = null,
        array $errors = [],
        int $statusCode = 200
    ) {
        return new StandardHttpResponse($status, $data, $message, $errors, $statusCode);
    }
}
