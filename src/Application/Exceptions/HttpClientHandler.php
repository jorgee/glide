<?php

namespace Glide\application\Exceptions;

use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Responses\StandardHttpResponse;
use Glide\Client\Exceptions\HttpClientException;
use Illuminate\Http\JsonResponse;

/**
 * Class HttpClientHandler
 * @package App\Exceptions
 */
class HttpClientHandler
{

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * HttpClientHandler constructor.
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }


    /**
     * Respond with the correct error message
     * @param HttpClientException $exception
     * @return JsonResponse
     */
    public function render(HttpClientException $exception)
    {
        $message = $exception->getMessage();
        $decoded = json_decode($message, true);
        $message = $decoded ?? $message;

        if (is_array($message) && array_key_exists('error', $message)) {
            $message = $message['error'];
        }

        if (!is_array($message)) {
            $message = [$message];
        }

        /** @var StandardHttpResponse $response */
        $response = $this->responseFactory::makeStandardResponse(
            false,
            [],
            $this->getResponseMessage($exception),
            $message,
            $exception->getHttpStatus()
        );

        return response()->json($response->getResponseArray(), $exception->getHttpStatus());
    }

    /**
     * Get the message according to Exception class type
     * @param HttpClientException $exception
     * @return string
     */
    private function getResponseMessage(HttpClientException $exception)
    {
        switch (get_class($exception)) {
            default:
                return 'There was a problem during a request';
        }
    }
}
