<?php

namespace Glide\Application\Exceptions;

use Exception;
use Glide\Application\Responses\StandardHttpResponse;
use Glide\Client\Exceptions\HttpClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * @var JsonExceptionResponseBuilder $responseBuilder
     */
    private $responseBuilder;

    /**
     * @var HttpClientHandler
     */
    private $httpClientHandler;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Handler constructor.
     * @param HttpClientHandler $httpClientHandler
     * @param JsonExceptionResponseBuilder $responseBuilder
     */
    public function __construct(HttpClientHandler $httpClientHandler, JsonExceptionResponseBuilder $responseBuilder)
    {
        $this->httpClientHandler = $httpClientHandler;
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response|JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof HttpClientException) {
            return $this->httpClientHandler->render($exception);
        }

        /** @var StandardHttpResponse $response */
        $response = $this->responseBuilder->build($exception);

        return response()->json(
            $response->getResponseArray(),
            $response->getHttpCode()
        );
    }
}
