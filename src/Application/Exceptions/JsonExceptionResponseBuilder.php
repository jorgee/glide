<?php

declare(strict_types = 1);

namespace Glide\application\Exceptions;

use Exception;
use Glide\Application\Responses\ResponseFactory;
use Glide\Application\Responses\StandardHttpResponse;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class JsonExceptionResponseBuilder
{
    /**
     * @var ResponseFactory The Standard Response Factory.
     */
    protected $responseFactory;

    /**
     * JsonExceptionResponseBuilder constructor.
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * Build the json response array using StandardJsonResponse
     *
     * @param Exception $exception
     * @return StandardHttpResponse
     */
    public function build(Exception $exception): StandardHttpResponse
    {
        $code = $this->defineHttpCode($exception);

        $response = $this->responseFactory::makeStandardResponse(
            false,
            [],
            Response::$statusTexts[$code],
            $this->defineMessages($exception),
            $code
        );

        return $response;
    }

    /**
     * define Http code
     *
     * @param Exception $exception
     * @return int
     */
    public function defineHttpCode(Exception $exception): int
    {
        $code = $this->isValidHttpCode((int)$exception->getCode()) ?
            $exception->getCode() :
            Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($exception instanceof NotFoundHttpException) {
            $code = Response::HTTP_NOT_FOUND;
        } elseif ($exception instanceof ValidationException) {
            $code = Response::HTTP_BAD_REQUEST;
        }

        return $code;
    }


    /**
     * Check that every exception contains valid HTTP code. Otherwise, set HTTP 500
     *
     * @param int $code
     * @return boolean
     */
    private function isValidHttpCode(int $code): bool
    {
        return array_key_exists($code, Response::$statusTexts);
    }

    /**
     * Define message to show in response, ensuring human readable message
     *
     * @param Exception $exception
     * @return array
     */
    private function defineMessages(Exception $exception): array
    {
        $message = collect($exception->getMessage());

        if ($exception instanceof ValidationException) {
            $message = collect($exception->errors())->flatten();
        }

        $message = $message->filter();
        if ($message->isEmpty()) {
            $message->add("Error: " . get_class($exception));
        }

        return $message->toArray();
    }
}
