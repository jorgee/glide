<?php

namespace Glide\Application\Requests\External;

/**
 * Class OfficeDetailFormRequest
 * @package Glide\Application\Requests\External
 */
class OfficeDetailFormRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        return [
            'expand' => 'sometimes|required|array',
            'expand.*' => 'regex:/^[a-z]+(\.?[a-z]+)+$/',//ensure that param comes in the form xxx, xxx.yyy, xxx.yyy.zzz
        ];
    }
}
