<?php

namespace Glide\Application\Requests\External;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;

abstract class FormRequest extends Request
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @throws ValidationException
     */
    public function validate()
    {
        if ($this->authorize() === false) {
            throw new UnauthorizedException();
        }

        /** @var Factory $validator */
        $validator = app('validator');
        $validator = $validator->make($this->all(), $this->rules(), $this->messages());

        $this->withValidator($validator);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->validator = $validator;
    }

    /**
     * Get the validated data from the request.
     *
     * @return array
     */
    public function validated()
    {
        return $this->validator->validated();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract protected function rules(): array;

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    protected function messages(): array
    {
        return [];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     *
     * @return void
     */
    protected function withValidator(Validator $validator): void
    {
        // Manipulate validator if needed
    }
}
