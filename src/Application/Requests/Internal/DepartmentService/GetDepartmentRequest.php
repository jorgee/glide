<?php

namespace Glide\Application\Requests\Internal\DepartmentService;

/**
 * Class GetDepartmentRequest
 * @package Glide\Application\Requests\Internal
 */
class GetDepartmentRequest
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var array
     */
    protected $expansions = [];

    /**
     * GetOfficeRequest constructor.
     * @param int $id
     * @param array $expansions
     */
    public function __construct(int $id, array $expansions)
    {
        $this->id = $id;
        $this->expansions = $expansions;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getExpansions(): array
    {
        return $this->expansions;
    }
}
