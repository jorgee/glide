<?php

namespace Glide\Application\Requests\Internal\OfficeService;

/**
 * Class GetAllOfficesRequest
 * @package Glide\Application\Requests\Internal
 */
class GetAllOfficesRequest
{
    /**
     * @var int
     */
    protected $offset;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var array
     */
    protected $expansions = [];

    /**
     * GetAllOfficesRequest constructor.
     * @param int $offset
     * @param int $limit
     * @param array $expansions
     */
    public function __construct(int $offset, int $limit, array $expansions)
    {
        $this->offset = $offset;
        $this->limit = $limit;
        $this->expansions = $expansions;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return array
     */
    public function getExpansions(): array
    {
        return $this->expansions;
    }
}
