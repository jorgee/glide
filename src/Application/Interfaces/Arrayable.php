<?php

namespace Glide\Application\Interfaces;

interface Arrayable
{
    public function toArray(): array;
}
