<?php

namespace Glide\Application\Interfaces;

interface HttpResponse
{

    /**
     * @return array The response properties all together.
     */
    public function getResponseArray(): array;

    /**
     * @return int The operation result code.
     */
    public function getHttpCode(): int;

    /**
     * @return bool The operation result status.
     */
    public function getSuccess(): bool;

    /**
     * @return string[] The errors, if any.
     */
    public function getErrors(): array;

    /**
     * @return array The extra data, if any.
     */
    public function getData(): array;

    /**
     * @return string The message, if any.
     */
    public function getMessage(): string;

    /**
     * Sets the response attributes
     *
     * @param bool $success
     * @param array $data
     * @param string $message
     * @param array $errors
     * @param int $statusCode
     *
     * @return self
     */
    public function setAttributes($success = false, $data = [], $message = '', $errors = [], $statusCode = 200): self;

    /**
     * Add error to errors array
     * @param string $errorMessage
     *
     * @return self
     */
    public function addError(string $errorMessage): self;

    /**
     * @param string[] $errors
     * @return HttpResponse
     */
    public function setErrors(array $errors): self;

    /**
     * @param array $data
     * @return HttpResponse
     */
    public function setData(array $data): self;

    /**
     * Set status code
     * @param int $statusCode
     *
     * @return self
     */
    public function setHttpCode(int $statusCode): self;

    /**
     * Set status
     * @param bool $status
     *
     * @return self
     */
    public function setSuccess(bool $status): self;

    /**
     * Set message
     * @param string $message
     *
     * @return self
     */
    public function setMessage(string $message): self;

    /**
     * @return string The response timestamp
     */
    public function getTimestamp(): string;
}
