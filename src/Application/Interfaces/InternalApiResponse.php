<?php

namespace Glide\Application\Interfaces;

interface InternalApiResponse
{
    /**
     * @return bool The operation result status.
     */
    public function getSuccess(): bool;

    /**
     * @return int The error code.
     */
    public function getErrorCode(): int;

    /**
     * @return string[] The errors, if any.
     */
    public function getErrors(): array;

    /**
     * @return mixed The extra data, if any.
     */
    public function getData();

    /**
     * @return string The message, if any.
     */
    public function getMessage(): string;

    /**
     * Sets the response attributes
     *
     * @param bool $status
     * @param array $data
     * @param string $message
     * @param array $errors
     *
     * @return InternalApiResponse
     */
    public function setAttributes($status = false, $data = [], $message = '', $errors = []): self;

    /**
     * Add error to errors array
     * @param string $errorMessage
     *
     * @return self
     */
    public function addError(string $errorMessage): self;

    /**
     * @param string[] $errors
     * @return InternalApiResponse
     */
    public function setErrors(array $errors): self;

    /**
     * @param mixed $data
     * @return InternalApiResponse
     */
    public function setData($data): self;

    /**
     * Set status
     * @param bool $status
     *
     * @return self
     */
    public function setSuccess(bool $status): self;

    /**
     * Set message
     * @param string $message
     *
     * @return self
     */
    public function setMessage(string $message): self;

    /**
     * @return string The response timestamp
     */
    public function getTimestamp(): string;
}
