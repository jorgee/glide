<?php

namespace Glide\Application\Responses;

use Glide\Application\Interfaces\HttpResponse;
use InvalidArgumentException;

/**
 * This class defines a standard response that can be used across any controller
 * to standardize the output of the requests.
 *
 * Class StandardResponse
 *
 */
class StandardHttpResponse implements HttpResponse
{
    /**
     * Status of the response. True in case of success or false in case of failure
     *
     * @var bool
     */
    private $success;

    /**
     * Array that contains the response data
     *
     * @var array
     */
    private $data;

    /**
     * Message that will be send containing any additional information about the response
     *
     * @var string
     */
    private $message;

    /**
     * Array that contains any error thrown on the response
     *
     * @var array
     */
    private $errors;

    /**
     * Http status code of the response. Default is 200.
     *
     * @var int
     */
    private $httpCode = 200;

    /**
     * @var string The response timestamp.
     */
    private $timestamp;

    public function __construct($success = false, $data = [], $message = null, $errors = [], $httpCode = 200)
    {
        $this->success = $success;
        $this->data = $data;
        $this->message = $message;
        $this->httpCode = $httpCode;
        $this->timestamp = date("c");
        $this->setErrors($errors);
    }

    /**
     * @return array
     */
    public function getResponseArray(): array
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
            'data' => $this->data,
            'errors' => $this->errors,
            'timestamp' => $this->timestamp,
        ];
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @param bool $success
     * @param array $data
     * @param string $message
     * @param array $errors
     * @param int $statusCode
     * @return HttpResponse
     */
    public function setAttributes(
        $success = false,
        $data = [],
        $message = '',
        $errors = [],
        $statusCode = 200
    ): HttpResponse {
        $this->success = $success;
        $this->data = $data;
        $this->message = $message;
        $this->httpCode = $statusCode;
        $this->setErrors($errors);

        return $this;
    }

    /**
     * @param string $errorMessage
     * @return HttpResponse
     */
    public function addError(string $errorMessage): HttpResponse
    {
        $this->errors[] = $errorMessage;
        return $this;
    }

    /**
     * @param int $httpCode
     * @return HttpResponse
     */
    public function setHttpCode(int $httpCode): HttpResponse
    {
        $this->httpCode = $httpCode;
        return $this;
    }

    /**
     * @param bool $success
     * @return HttpResponse
     */
    public function setSuccess(bool $success): HttpResponse
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @param string $message
     * @return HttpResponse
     */
    public function setMessage(string $message): HttpResponse
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool The operation result status.
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return string[] The errors, if any.
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return array The extra data, if any.
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string The message, if any.
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string[] $errors
     * @return HttpResponse
     */
    public function setErrors(array $errors): HttpResponse
    {
        foreach ($errors as $error) {
            if (!is_string($error)) {
                throw new InvalidArgumentException('Parameter $errors must be an array of strings.');
            }
        }
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param array $data
     * @return HttpResponse
     */
    public function setData(array $data): HttpResponse
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string The response timestamp
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }
}
