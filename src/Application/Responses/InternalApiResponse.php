<?php

namespace Glide\Application\Responses;

use Glide\Application\Interfaces\Arrayable;
use Glide\Application\Interfaces\InternalApiResponse as InternalApiResponseInterface;
use InvalidArgumentException;

/**
 * This class defines a standard response that can be used across any class.
 */
class InternalApiResponse implements InternalApiResponseInterface, Arrayable
{
    /**
     * Status of the response. True in case of success or false in case of failure
     *
     * @var bool
     */
    protected $success;

    /**
     * Response data
     *
     * @var mixed
     */
    protected $data;

    /**
     * Message that will be send containing any additional information about the response
     *
     * @var string
     */
    protected $message;

    /**
     * @var int
     */
    protected $errorCode;

    /**
     * Array that contains any error thrown on the response
     *
     * @var array
     */
    protected $errors;

    /**
     * @var string The response timestamp.
     */
    protected $timestamp;

    /**
     * @var InternalApiResponseInterface|null
     */
    protected $previousResponse;

    /**
     * InternalApiResponse constructor.
     * @param bool $status
     * @param array $data
     * @param null $message
     * @param int $errorCode
     * @param array $errors
     * @param InternalApiResponseInterface|null $previousResponse
     */
    public function __construct(
        $status = false,
        $data = null,
        $message = null,
        int $errorCode = 0,
        $errors = [],
        InternalApiResponseInterface $previousResponse = null
    ) {
        $this->success = $status;
        $this->data = $data;
        $this->message = $message;
        $this->timestamp = date("c");
        $this->errorCode = $errorCode;
        $this->setErrors($errors);
        $this->previousResponse = $previousResponse;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'success' => $this->getSuccess(),
            'message' => $this->getMessage(),
            'data' => $this->getData(),
            'errorCode' => $this->getErrorCode(),
            'errors' => $this->getErrors(),
            'timestamp' => $this->getTimestamp(),
        ];
    }

    /**
     * @param bool $status
     * @param array $data
     * @param string $message
     * @param array $errors
     * @return InternalApiResponse
     */
    public function setAttributes(
        $status = false,
        $data = [],
        $message = '',
        $errors = []
    ): InternalApiResponseInterface {
        $this->success = $status;
        $this->data = $data;
        $this->message = $message;
        $this->setErrors($errors);

        return $this;
    }

    /**
     * @param string $errorMessage
     * @return InternalApiResponse
     */
    public function addError(string $errorMessage): InternalApiResponseInterface
    {
        $this->errors[] = $errorMessage;
        return $this;
    }

    /**
     * @param bool $success
     * @return InternalApiResponse
     */
    public function setSuccess(bool $success): InternalApiResponseInterface
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @param string $message
     * @return InternalApiResponse
     */
    public function setMessage(string $message): InternalApiResponseInterface
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool The operation result status.
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * @return InternalApiResponseInterface|null
     */
    public function getPreviousResponse(): ?InternalApiResponseInterface
    {
        return $this->previousResponse;
    }

    /**
     * @return string[] The errors, if any.
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return mixed The extra data, if any.
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string The message, if any.
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode(int $errorCode): void
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @param InternalApiResponseInterface|null $previousResponse
     */
    public function setPreviousResponse(?InternalApiResponseInterface $previousResponse): void
    {
        $this->previousResponse = $previousResponse;
    }

    /**
     * @param string[] $errors
     * @return InternalApiResponse
     */
    public function setErrors(array $errors): InternalApiResponseInterface
    {
        foreach ($errors as $error) {
            if (!is_string($error)) {
                throw new InvalidArgumentException('Parameter $errors must be an array of strings.');
            }
        }
        $this->errors = $errors;
        return $this;
    }

    /**
     * @param $data
     * @return InternalApiResponse
     */
    public function setData($data): InternalApiResponseInterface
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string The response timestamp
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }
}
