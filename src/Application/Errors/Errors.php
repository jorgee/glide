<?php

namespace Glide\Application\Errors;

class Errors
{
    public const ERR_NO_ERROR = 0;
    public const ERR_NO_ERROR_MESSAGE = 'Success';

    public const ERR_NOT_FOUND_CODE = 1;
    public const ERR_NOT_FOUND_MESSAGE = 'Not found.';

    public const ERR_INVALID_EXPANSION_CODE = 2;
    public const ERR_INVALID_EXPANSION_CODE_MESSAGE = 'Invalid expansion requested.';
}
