<?php

return [
    'employee' => [
        'department' => 'department',
        'manager' => 'employee',
        'office' => 'office'
    ],
    'office' => null,
    'department' => [
        'superdepartment' => 'department'
    ]
];
