<?php

return [
    'departments_json' => __DIR__ . '/../storage/departments.json',
    'offices_json' => __DIR__ . '/../storage/offices.json',
    'employees_api_base_url' => 'https://rfy56yfcwk.execute-api.us-west-1.amazonaws.com',
];
