<?php

/** @var Laravel\Lumen\Routing\Router $router */
$router->group(['middleware' => 'param.expand.parser'], function () use ($router) {
    $router->get('/employees', 'Employees@index');
    $router->get('/employees/{id}', 'Employees@getById');
    $router->get('/departments', 'Departments@index');
    $router->get('/departments/{id}', 'Departments@getById');
    $router->get('/offices', 'Offices@index');
    $router->get('/offices/{id}', 'Offices@getById');
});
