# Glide

To run the application:

`cd` to the application directory.

do `docker-compose up -d`

App should run on `http://localhost:8080`

Test with Postman, for example, `http://localhost:8080/employees?expand=office&expand=department.superdepartment&expand=manager.department.superdepartment`

Application has no test suite because it was not required and added time. However a TDD would have been nice to avoid extra work lol.